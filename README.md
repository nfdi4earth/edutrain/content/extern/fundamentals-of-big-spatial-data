<meta name="author" content="Farzaneh Sadeghi, Hydrogeology Modeling Group (KIT)"/>
<meta name="subjectArea" content="Geoinformatics, Climate Science, Earth System Science, Hydrology"/>

<section class="about" style="font-family: Calibri, Arial, Helvetica, sans-serif; text-align: justify;">
<h2>About This Course</h2>
<p>The "Fundamentals of Big Spatial Data" course introduces participants to the essential techniques and tools required for managing, analyzing, and interpreting large-scale spatial datasets. Through a series of hands-on lessons, participants will explore topics such as working with Python for spatial data, handling time series, database management, and leveraging platforms like Google Earth Engine for advanced spatial analysis. The course combines theoretical insights with practical exercises, enabling participants to apply learned skills directly to real-world datasets.</p>
</section>

<section class="difficulty" style="font-family: Calibri, Arial, Helvetica, sans-serif; text-align: justify;">
<h2>Level</h2>
<p>Intermediate</p>
</section>

<section class="prerequisites" style="font-family: Calibri, Arial, Helvetica, sans-serif; text-align: justify;">
<h2>Requirements</h2>
<p>Prior knowledge of geostatistics is required.</p>
</section>

<section class="subjectArea" style="font-family: Calibri, Arial, Helvetica, sans-serif; text-align: justify;">
<h2>Subject Area</h2>
<p>Geoinformatics, Climate Science, Earth System Science, Hydrology</p>
</section>
  
<section class="content-overview" style="font-family: Calibri, Arial, Helvetica, sans-serif; text-align: justify;">
<h2>What You Will Learn</h2>
<ul>
  <li>Big spatial data and Earth system science</li>
  <li>Setting up Python to work with big spatial data</li>
  <li>Handling date and time</li>
  <li>Reading and analyzing time series</li>
  <li>Frequency analysis of time series</li>
  <li>Fourier analysis</li>
  <li>Big data introduction</li>
  <li>Xarray</li>
  <li>Databases and Structured Query Language (coming soon)</li>
  <li>Advanced handling of databases (coming soon)</li>
  <li>Introduction to Google Earth Engine (coming soon)</li>
  <li>Analyze and export using Google Earth Engine (coming soon)</li>
</ul>
</section>

<section class="credits" style="font-family: Calibri, Arial, Helvetica, sans-serif; text-align: justify;">
<h2>Resources</h2>
<p>
    <a href="https://github.com/KITHydrogeology/Geodatenanalyse-II?tab=readme-ov-file" target="_blank">Geodatenanalyse II</a> by Hydrogeology Modeling Group at KIT<br>Farzaneh Sadeghi
</p>
<h2>Administration</h2>
<p>Farzaneh Sadeghi</p>
</section>

<footer style="color: black !important; text-align: justify !important; font-size: 0.7em !important; font-family: Calibri, Arial, Helvetica, sans-serif !important;">
    <hr style="border-top: 2px solid #ccc; width: 50%; margin-left: 0;">
    <p>
        This content is based on "<a href="https://github.com/KITHydrogeology/Geodatenanalyse-II?tab=readme-ov-file" style="color: black !important; text-decoration: underline !important;">Geodatenanalyse II</a>" by Hydrogeology Modeling Group at KIT, which is licensed under a <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" style="color: black !important; text-decoration: underline !important;">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>. Modifications were made to the original content. This modified content is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" style="color: black !important; text-decoration: underline !important;">CC BY-NC-SA 4.0</a>.
    </p>
</footer>