{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 📖Setting up Python to work with big spatial data\n",
    "\n",
    "Let's start by downloading the necessary dependencies:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "%pip install xlrd # xlrd is a library for reading Excel files in Python.\n",
    "%pip install openpyxl # openpyxl is a library for reading and writing Excel files in Python.\n",
    "%pip install ipykernel # ipykernel is a library for running Python code in Jupyter notebooks.\n",
    "%pip install mpmath # mpmath is a library for arbitrary-precision arithmetic in Python.\n",
    "%pip install sympy # sympy is a library for symbolic mathematics in Python.\n",
    "%pip install xarray # xarray is a library for working with labeled multi-dimensional arrays in Python.\n",
    "%pip install netCDF4 # netCDF4 is a library for working with netCDF files in Python.\n",
    "%pip install seaborn # seaborn is a library for creating statistical graphics in Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Install additional spatial packages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "%pip install gdal # GDAL is a library for reading and writing raster and vector geospatial data formats.\n",
    "%pip install shapely # Shapely is a library for creating, manipulating, and analyzing planar objects in Python.\n",
    "%pip install cartopy # Cartopy is a library for creating maps and visualizing geospatial data in Python.\n",
    "%pip install descartes # Descartes is a library for plotting shapely geometric objects in Python.\n",
    "%pip install contextily # Contextily is a library for fetching basemap tiles from tile providers. \n",
    "%pip install SALib # SALib is a library for sensitivity analysis in Python.\n",
    "%pip install pykrige # PyKrige is a library for geostatistical analysis in Python.\n",
    "%pip install ruptures # Ruptures is a library for change point detection in Python.\n",
    "%pip install geopandas # GeoPandas is a library for working with geospatial data in Python.\n",
    "%pip install jupyterlab # JupyterLab is an interactive development environment for Python.\n",
    "%pip install earthengine-api # Earth Engine API is a library for accessing Google Earth Engine's data and services in Python.\n",
    "%pip install geemap # Geemap is a library for working with Google Earth Engine in Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test out GeoPandas and see if it works!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'3060200'"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from osgeo import gdal, ogr, osr\n",
    "from fiona.ogrext import Iterator, ItemsIterator, KeysIterator\n",
    "from geopandas import GeoDataFrame\n",
    "gdal.VersionInfo()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If this test doesn't show any error message, then we're all good to go!🥳\n",
    "\n",
    "If not, don't worry, try using  conda to install the GDAL package, as it simplifies managing dependencies and configuration:\n",
    "\n",
    "```\n",
    "conda install -c conda-forge gdal\n",
    "conda install -c conda-forge fiona geopandas\n",
    "```\n",
    "If you get conflict between PROJ libraries, you can try this:\n",
    "\n",
    "```\n",
    "conda install -y -c conda-forge gdal\n",
    "conda install -y -c conda-forge proj\n",
    "```\n",
    "\n",
    "💡If you want to to run conda commands directly in a notebook cell, use the `%` magic command, e.g., `conda install -c conda-forge gdal`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Google Earth Engine is a powerful tool for processing and analyzing large-scale spatial data. Use the following instructions for authorizing Google Earth Engine. You will need a Google account, the same type used for Gmail or Maps. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ee"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before you can use the Earth Engine API, you need to authenticate your access, which involves verifying your credentials and setting up access to your Google account. You need to go through the authorization process once; proceed with the following steps and follow all provided instructions from Google to complete the authorization process. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# autheticate the Earth Engine module\n",
    "ee.Authenticate()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify authorization by executing the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NASA SRTM Digital Elevation 30m\n"
     ]
    }
   ],
   "source": [
    "import ee\n",
    "\n",
    "# Initialize the Earth Engine module.\n",
    "ee.Initialize()\n",
    "\n",
    "# Test the connection\n",
    "print(ee.Image('USGS/SRTMGL1_003').get(\"title\").getInfo())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above line initializes the Earth Engine module. It establishes a connection between the local environment and the GEE servers, making it possible to perform operations using GEE.\n",
    "\n",
    "The initialization should be successful and error-free (if not, please visit this [page!](https://code.earthengine.google.com/register)). If there are no errors, it confirms that the user is properly authorized and that the environment is correctly set up.\n",
    "\n",
    "The following line of code retrieves and prints metadata for a specific DEM dataset from GEE (in this case, the SRTMGL1 DEM dataset provided by USGS). The output includes information about the dataset, such as its properties, bands, and other metadata, verifying the functionality of GEE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'type': 'Image', 'bands': [{'id': 'elevation', 'data_type': {'type': 'PixelType', 'precision': 'int', 'min': -32768, 'max': 32767}, 'dimensions': [1296001, 417601], 'crs': 'EPSG:4326', 'crs_transform': [0.0002777777777777778, 0, -180.0001388888889, 0, -0.0002777777777777778, 60.00013888888889]}], 'version': 1641990767055141, 'id': 'USGS/SRTMGL1_003', 'properties': {'system:visualization_0_min': '0.0', 'type_name': 'Image', 'keywords': ['dem', 'elevation', 'geophysical', 'nasa', 'srtm', 'topography', 'usgs'], 'thumb': 'https://mw1.google.com/ges/dd/images/SRTM90_V4_thumb.png', 'description': '<p>The Shuttle Radar Topography Mission (SRTM, see <a href=\"https://onlinelibrary.wiley.com/doi/10.1029/2005RG000183/full\">Farr\\net al. 2007</a>)\\ndigital elevation data is an international research effort that\\nobtained digital elevation models on a near-global scale. This\\nSRTM V3 product (SRTM Plus) is provided by NASA JPL\\nat a resolution of 1 arc-second (approximately 30m).</p><p>This dataset has undergone a void-filling process using open-source data\\n(ASTER GDEM2, GMTED2010, and NED), as opposed to other versions that\\ncontain voids or have been void-filled with commercial sources.\\nFor more information on the different versions see the\\n<a href=\"https://lpdaac.usgs.gov/documents/13/SRTM_Quick_Guide.pdf\">SRTM Quick Guide</a>.</p><p>Documentation:</p><ul><li><p><a href=\"https://lpdaac.usgs.gov/documents/179/SRTM_User_Guide_V3.pdf\">User&#39;s Guide</a></p></li><li><p><a href=\"https://lpdaac.usgs.gov/documents/13/SRTM_Quick_Guide.pdf\">General Documentation</a></p></li><li><p><a href=\"https://doi.org/10.1029/2005RG000183\">Algorithm Theoretical Basis Document (ATBD)</a></p></li></ul><p><b>Provider: <a href=\"https://cmr.earthdata.nasa.gov/search/concepts/C1000000240-LPDAAC_ECS.html\">NASA / USGS / JPL-Caltech</a></b><br><p><b>Bands</b><table class=\"eecat\"><tr><th scope=\"col\">Name</th><th scope=\"col\">Description</th></tr><tr><td>elevation</td><td><p>Elevation</p></td></tr></table><p><b>Terms of Use</b><br><p>Unless otherwise noted, images and video on JPL public\\nweb sites (public sites ending with a jpl.nasa.gov address) may\\nbe used for any purpose without prior permission. For more information\\nand exceptions visit the <a href=\"https://www.jpl.nasa.gov/imagepolicy/\">JPL Image Use Policy site</a>.</p><p><b>Suggested citation(s)</b><ul><li><p>Farr, T.G., Rosen, P.A., Caro, E., Crippen, R., Duren, R., Hensley,\\nS., Kobrick, M., Paller, M., Rodriguez, E., Roth, L., Seal, D.,\\nShaffer, S., Shimada, J., Umland, J., Werner, M., Oskin, M., Burbank,\\nD., and Alsdorf, D.E., 2007, The shuttle radar topography mission:\\nReviews of Geophysics, v. 45, no. 2, RG2004, at\\n<a href=\"https://doi.org/10.1029/2005RG000183\">https://doi.org/10.1029/2005RG000183</a>.</p></li></ul><style>\\n  table.eecat {\\n  border: 1px solid black;\\n  border-collapse: collapse;\\n  font-size: 13px;\\n  }\\n  table.eecat td, tr, th {\\n  text-align: left; vertical-align: top;\\n  border: 1px solid gray; padding: 3px;\\n  }\\n  td.nobreak { white-space: nowrap; }\\n</style>', 'source_tags': ['nasa', 'usgs'], 'visualization_0_max': '6000.0', 'title': 'NASA SRTM Digital Elevation 30m', 'product_tags': ['srtm', 'elevation', 'topography', 'dem', 'geophysical'], 'provider': 'NASA / USGS / JPL-Caltech', 'visualization_0_min': '0.0', 'visualization_0_name': 'Elevation', 'date_range': [950227200000, 951177600000], 'system:visualization_0_gamma': '1.6', 'period': 0, 'system:visualization_0_bands': 'elevation', 'provider_url': 'https://cmr.earthdata.nasa.gov/search/concepts/C1000000240-LPDAAC_ECS.html', 'visualization_0_gamma': '1.6', 'sample': 'https://mw1.google.com/ges/dd/images/SRTM90_V4_sample.png', 'tags': ['dem', 'elevation', 'geophysical', 'nasa', 'srtm', 'topography', 'usgs'], 'system:visualization_0_max': '6000.0', 'system:visualization_0_name': 'Elevation', 'system:asset_size': 132792638252, 'visualization_0_bands': 'elevation'}}\n"
     ]
    }
   ],
   "source": [
    "# Print metadata for a DEM dataset.\n",
    "print(ee.Image('USGS/SRTMGL1_003').getInfo())"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "LMS",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.18"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
